"""Query all data"""

import sqlite3

CONN = sqlite3.connect("outputs/combined_data.db")

CURSOR = CONN.cursor()

REQUEST = input("Enter '1' to query all data or enter '2' to enter your own query: ")

if REQUEST == "1":
    QUERY = "SELECT * FROM combined_data"
else:
    QUERY = input("Please write your query here: ")

CURSOR.execute(QUERY)

results = CURSOR.fetchall()

for row in results:
    print(row)

CONN.close()
