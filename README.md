# Seattle SuperSonics Fan Data

This project aims to create a unified database table from various data sources related to the Seattle SuperSonics fans. The table will be used to build fan segments and determine their behaviors.

## Table of Contents

- [Getting Started](#getting-started)
- [Data Sources](#data-sources)
- [Data Processing](#data-processing)
- [Output](#output)
- [Running the Code](#running-the-code)
- [Initial Insights](#initial-insights)
- [Future Improvements](#future-improvements)

## Getting Started

These instructions will help you set up the project and run the code on your local machine.

### Prerequisites

You will need Python 3.7+ installed on your machine. You can download it from [Python's official website](https://www.python.org/downloads/).

### Installing

Clone the repository or download the provided code. Install the required packages using pip:

```
pip install -r requirements.txt
```

Typically when using Python you use a virtual environment. However, given the limited number of packages and the frequency with which Pandas is used, it may make more sense to directly install the packages.

## Data Sources

The following data sources are used in this project:

- Ticket transaction data (tickets.csv)
- Retail transaction data (retail.json)
- Fan survey demographic data (surveys.csv)

A data dictionary is provided in the project folder (data_dictionary.xlsx) with detailed information about each data source and its fields.

## Data Processing

The processing of the data involves the following steps:

1. Load the data from the provided files.
2. Check the data for any errors or inconsistencies.
3. Clean the data to remove any errors or inconsistencies.
4. Aggregate the data to create calculated fields.
5. Merge the data from different sources into a single table.
6. Create a unique identifier for each fan.
7. Reorder the columns for better readability.
8. Save the combined data to a CSV file and SQLite database.

## Output

The output is a combined data table with the following fields:

- id
- email
- phone_no
- zip
- number_of_ticket_transactions
- number_of_retail_transactions
- number_of_survey_responses
- games_attended
- back_office_spend
- box_office_spend
- web_spend
- total_ticket_spend
- total_retail_spend(excl. shipping)
- total_spend(excl. shipping)
- future_attendance_likelihood(avg)
- unique_product_types_ordered

The output is saved as a CSV file (combined_data.csv) and an SQLite database (combined_data.db).

## Running the Code

To run the code, open a terminal or command prompt, navigate to the project folder, and run the following command:

```
python main.py
```

The code will process the data and save the output to the specified files.

## Initial Insights:

- Box office ticket transactions likely have lower margins for the organization since there are additional labor costs (assuming no price discrimination). Therefore the sales and marketing teams could use the "Box Office" column to offer discount codes to fans who have large transaction values/ a large percentage of sales to the box office channel to drive them towards web sales. Such fans include user490@supersonics.com and user439@supersonics.com.
- Using the "List of Unique Product Types Ordered" column, the retail team could automate discount codes so they are sent after a transaction occurs. Discount codes could be for products the fan did not purchase. (e.g. after user343@supersonics.com purchased his Jersey we could send him a discount code for a hat.)
- Using the "Number of Ticket Transactions" and the "Future attendance likelihood" columns the sales and marketing team could target fans who said they were likely to return but who have attended a low number of games. (Compared to others who reported a high average likelihood to return, fans user423@supersonics.com and user473@supersonics.com
have attended a relatively low number of games.)

## Future Improvements:

In the future, if this report was not a one-time generation I would add a testing module to ensure accuracy. For now, taking a random sampling of output data manually combined with the "check_data" function (which checks the inputs for errors) provides a reasonable level of confidence in the output. Other improvements include additional or customized columns based on business needs.
