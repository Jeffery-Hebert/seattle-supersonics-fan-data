""" Seattle SuperSonics technical challenge """

import json
import hashlib
import re
import pandas as pd
import numpy as np
from sqlalchemy import create_engine

# import requests

# API_KEY = "**NEEDS TO BE FILLED**"


def load_data():
    """Pull in the data"""
    surveys_df = pd.read_csv("provided files/surveys.csv")
    tickets_df = pd.read_csv("provided files/tickets.csv")
    retail_df = pd.read_json("provided files/retail.json")
    retail_df = pd.json_normalize(retail_df["retail"])

    # Pivot from long form to wide form for better use
    surveys_df = surveys_df.pivot(
        index="Submission ID", columns="Attribute", values="Value"
    )

    return surveys_df, tickets_df, retail_df


def check_data(data_frame):
    """Checking to make sure there are no errors in the data."""

    if data_frame.isnull().sum().sum() > 0:
        print("There are null values in the data.")
        print(data_frame.isnull().sum())

    if data_frame.duplicated().sum() > 0:
        print("There are duplicated rows in the data.")
        print(data_frame.duplicated().sum())

    acceptable_types = [
        np.dtype("int64"),
        np.dtype("float64"),
        np.dtype("O"),
        np.dtype("<M8[ns]"),
    ]

    for column in data_frame.columns:
        if data_frame[column].dtype not in acceptable_types:
            print(f"There are unexpected data types in column {column}.")

    if "phone_no" in data_frame.columns:
        phone_pattern = r"^\d{3}-\d{3}-\d{4}$"
        invalid_phones_mask = data_frame["phone_no"].apply(
            lambda x: re.match(phone_pattern, str(x)) is None
        )
        if invalid_phones_mask.sum() > 0:
            print("There are incorrectly formatted phone numbers in the data:")
            print(data_frame.loc[invalid_phones_mask, "phone_no"])

    if "email" in data_frame.columns:
        email_pattern = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"
        invalid_emails = data_frame["email"].apply(
            lambda x: re.match(email_pattern, str(x)) is None
        )
        if invalid_emails.sum() > 0:
            print("There are incorrectly formatted email addresses in the data.")
            print(data_frame.loc[invalid_emails, "email"])

    if (data_frame.select_dtypes(include=[object]) == "").sum().sum() > 0:
        print("There are empty strings in the data.")


def clean_data(surveys_df, tickets_df):
    """
    Within surveys.csv values of 1 and 5 have descriptions.
    For ease in the calculation of metrics, I decided to remove the description of the value.

    There is a four-digit zip code in tickets.csv.
    While theoretically, the fan could live in Sabangan Philippines,
    it is more likely that this was an error.
    I set the zip code to the very last zip code 99950.
    Converted zips to strings since they function more like categorical data rather than numerical.
    """

    def process_survey_value(value):
        if "-" in value:
            return int(value.split("-")[0].strip())
        return int(value)

    survey_attributes_to_clean = [
        "how_satisfied_were_you_with_this_event",
        "how_satisfied_were_you_with_your_retail_experience_at_this_event",
        "how_likely_are_you_to_attend_this_event_in_the_future",
    ]

    for survey_attribute in survey_attributes_to_clean:
        surveys_df[survey_attribute] = surveys_df[survey_attribute].apply(
            process_survey_value
        )

    def process_tickets_values(value):
        if len(str(value)) < 5:
            return "99950"
        return str(value)

    tickets_df["zip"] = tickets_df["zip"].apply(process_tickets_values)

    return surveys_df, tickets_df


def aggregate_data(surveys_df, tickets_df, retail_df):
    """
    Grouping the data and creating calculated figures. Prepping for merge
    """

    old_column = "how_likely_are_you_to_attend_this_event_in_the_future"
    new_column_names = {
        "phone_no": "number_of_survey_responses",
        old_column: "future_attendance_likelihood(avg)",
    }

    surveys_grouped = (
        surveys_df.groupby("phone_no")
        .agg(
            {
                "phone_no": "size",
                "how_likely_are_you_to_attend_this_event_in_the_future": "mean",
            }
        )
        .rename(columns=new_column_names)
        .reset_index()
    )

    tickets_grouped = (
        tickets_df.groupby("phone_no")
        .agg(
            {
                "phone_no": "size",
                "email": "first",
                "zip": "first",
                "total_price": "sum",
                "event_id": "nunique",
            }
        )
        .rename(
            columns={
                "phone_no": "number_of_ticket_transactions",
                "email": "email",
                "zip": "zip",
                "total_price": "total_ticket_spend",
                "event_id": "games_attended",
            }
        )
        .reset_index()
    )

    total_value_by_channel_and_fan = (
        tickets_df.groupby(["phone_no", "channel"])["total_price"].sum().reset_index()
    )

    total_value_by_channel_and_fan = total_value_by_channel_and_fan.pivot(
        index="phone_no", columns="channel", values="total_price"
    )

    retail_df["transaction_value"] = retail_df["quantity"] * retail_df["unit_price"]
    retail_grouped = (
        retail_df.groupby("email")
        .agg(
            {
                "email": "size",
                "transaction_value": "sum",
                "product_type": lambda x: list(x.unique()),
            }
        )
        .rename(
            columns={
                "email": "number_of_retail_transactions",
                "transaction_value": "total_retail_spend(excl. shipping)",
                "product_type": "unique_product_types_ordered",
            }
        )
        .reset_index()
    )

    return (
        surveys_grouped,
        tickets_grouped,
        retail_grouped,
        total_value_by_channel_and_fan,
    )


def merge_data(
    surveys_grouped, tickets_grouped, retail_grouped, total_value_by_channel_and_fan
):
    """Combining  the data"""
    combined_df = pd.merge(surveys_grouped, tickets_grouped, on="phone_no", how="outer")
    combined_df = pd.merge(combined_df, retail_grouped, on="email", how="outer")
    combined_df = pd.merge(
        combined_df, total_value_by_channel_and_fan, on="phone_no", how="outer"
    )

    combined_df.fillna(
        {
            "number_of_retail_transactions": 0,
            "number_of_survey_responses": 0,
            "Back Office": 0,
            "Box Office": 0,
            "Web": 0,
            "total_retail_spend(excl. shipping)": 0,
        },
        inplace=True,
    )

    combined_df["total_spend(excl. shipping)"] = (
        combined_df["total_ticket_spend"]
        + combined_df["total_retail_spend(excl. shipping)"]
    )

    return combined_df


def hash_emails_and_phone(row):
    """Unique identifier generation"""
    return hashlib.sha256((str(row["email"] + row["phone_no"])).encode()).hexdigest()


# def get_distance(data_frame):
#     """
#     Designed to calculate the distance from
#     the stadium to the center of the fans zipcode.
#     Not implemented in this version because the amount of data
#     slightly exceeds the maximum levels allowed
#     under the free plan.
#     However there are multiple cheap plans that would drive more value than their cost.
#     Could limit calls by only searching for unique zip codes.
#     """
#     if API_KEY != "**NEEDS TO BE FILLED**":
#         base_url = "https://www.zipcodeapi.com/rest"
#         fan_zip = data_frame["zip"]
#         stadium_zip = "98109"
#         full_url = f"{base_url}/{API_KEY}/distance.json/{fan_zip}/{stadium_zip}/mile"
#         response = requests.get(full_url, timeout=2)
#         response = response.json()
#         return response["distance"]
#     raise Exception("You must enter an API_KEY to use the get_distance function")


def prep_output(data_frame):
    """Reformat column order in a way that flows a bit better"""

    data_frame = data_frame.rename(
        columns={
            "BackOffice": "back_office_spend",
            "Box Office": "box_office_spend",
            "Web": "web_spend",
        }
    )

    data_frame = data_frame[
        [
            "id",
            "email",
            "phone_no",
            "zip",
            # "distance_from_stadium(mi)",
            "number_of_ticket_transactions",
            "number_of_retail_transactions",
            "number_of_survey_responses",
            "games_attended",
            "back_office_spend",
            "box_office_spend",
            "web_spend",
            "total_ticket_spend",
            "total_retail_spend(excl. shipping)",
            "total_spend(excl. shipping)",
            "future_attendance_likelihood(avg)",
            "unique_product_types_ordered",
        ]
    ]

    return data_frame


def save_data(
    data_frame,
    db_file_path="sqlite:///outputs/combined_data.db",
    csv_file_path="outputs/combined_data.csv",
    table_name="combined_data",
):
    """Output data to SQLite database and CSV file."""

    data_frame.to_csv(csv_file_path, index=False)
    print("Data saved to combined_data.csv")

    data_frame["unique_product_types_ordered"] = data_frame[
        "unique_product_types_ordered"
    ].apply(json.dumps)
    engine = create_engine(db_file_path)
    data_frame.to_sql(table_name, engine, if_exists="replace", index=False)
    print(f"Data saved to {db_file_path} in table {table_name}")


def main():
    """Main Function"""

    # Load the data
    surveys_df, tickets_df, retail_df = load_data()

    # Check the data before cleaning
    check_data(surveys_df)
    check_data(tickets_df)
    check_data(retail_df)

    # Clean the data
    surveys_df, tickets_df = clean_data(surveys_df, tickets_df)

    # Aggregate the data
    (
        surveys_grouped,
        tickets_grouped,
        retail_grouped,
        total_value_by_channel_and_fan,
    ) = aggregate_data(surveys_df, tickets_df, retail_df)

    # Merge the data
    combined_df = merge_data(
        surveys_grouped, tickets_grouped, retail_grouped, total_value_by_channel_and_fan
    )

    # Hash emails and phone_no
    combined_df["id"] = combined_df.apply(hash_emails_and_phone, axis=1)

    # # Find Distance From Stadium
    # combined_df["distance_from_stadium(mi)"] = combined_df.apply(get_distance, axis=1)

    # Reorder columns for better human consumption
    combined_df = prep_output(combined_df)

    # Save the data
    save_data(combined_df)


if __name__ == "__main__":
    main()
